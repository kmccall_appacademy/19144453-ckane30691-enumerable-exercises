require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.empty? ? 0: arr.inject(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  result = []
  long_strings.each {|word| result << substring if word.include?(substring)}
  result.length == long_strings.length ? true: false
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.split("").select {|letter| 
    string.count(letter) > 1 unless letter == " "}.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  biggest = ""
  next_biggest = ""
  string.split(" ").each do |word| 
    next_biggest, biggest = biggest, word if word.length > biggest.length
  end
  [biggest, next_biggest]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").select {|letter| !string.include?(letter)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|year| not_repeat_year?(year)}
end

def not_repeat_year?(year)
  year.to_s.split("").uniq == year.to_s.split("") ? true: false
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  result = songs
  (0...songs.length).each do |idx|
    prev_song = songs[idx - 1]
    next_song = songs[idx + 1]
    if prev_song == songs[idx] || next_song == songs[idx]
      result.delete(songs[idx])
    end
  end
  result.uniq
end

# def no_repeats?(song_name, songs)
# end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  c_hash = {}
  string.split(" ").each do |word|
    word = remove_punct(word)
    c_hash[word] = c_distance(word)
  end
  c_hash.key(c_hash.values.min)
end

def c_distance(word)
  word.include?("c") ? word.length - (word.index("c") + 1): 100
end

def remove_punct(word)
  word.split("").select {|letter| ("a".."z").include?(letter)}.join("")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  used = []
  (0...arr.length).each do |idx1|
    nested_arr = []
    prev_num = arr[idx1 - 1]
    if prev_num == arr[idx1] && used.last != arr[idx1]
      used << arr[idx1]
      nested_arr << idx1 - 1
      (idx1..arr.length).each do |idx2|
        if prev_num != arr[idx2]
          nested_arr << idx2 - 1
          break
        end
      end
    end
    result << nested_arr unless nested_arr.empty?
  end
  result
end
